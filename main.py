import csv
import os

WORD_OPTION_INDEX = 1
USER_OPTIONS_FILE_NAME = "user_options.txt"
NUMBER_OF_WORDS = 198 + 154 + 153


def create_user_options_file():
    """
    Creates the user's options file and updates in with every possible shiritori option
    :return: None
    """

    # reading only the shiritori words from the file
    complete_options_file = open("srt_panel.csv", "r")
    shiritori_reader = csv.reader(complete_options_file)

    # ignoring the title of the column
    next(shiritori_reader)

    # putting all the options in a list
    options = [line[WORD_OPTION_INDEX].lower() for line in shiritori_reader]

    # creating the user's file and putting all the options in it
    user_options_file = open(USER_OPTIONS_FILE_NAME, "w")
    for option in options:
        user_options_file.write(option + "\n")

    # closing resources
    complete_options_file.close()
    user_options_file.close()


def main():

    cls = lambda: os.system('cls')

    # creating the user's file, if it does not already exist
    if not os.path.isfile(USER_OPTIONS_FILE_NAME):
        create_user_options_file()

    # opening the user's file for reading + writing
    user_options_file = open(USER_OPTIONS_FILE_NAME, "r+")

    options = user_options_file.readlines()

    # counting how many options of each letter still remain
    letters_remaining_count = {'a':0, 'b':0, 'c':0, 'd':0, 'e':0, 'f':0, 'g':0, 'h':0, 'i':0, 'j':0, 'k':0, 'l':0, 'm':0, 'n':0, 'o':0, 'p':0, 'q':0, 'r':0, 's':0, 't':0, 'u':0, 'v':0, 'w':0, 'x':0, 'y':0, 'z':0}
    for option in options:
        letters_remaining_count[option[0]] += 1

    try:
        while True:

            # showing the user their progress
            completed_options_count = NUMBER_OF_WORDS - len(options)
            print("Completed {0}/{1} ({2}%)".format(completed_options_count, NUMBER_OF_WORDS, round((completed_options_count / NUMBER_OF_WORDS) * 100, 4)))
            print("Completed letters:", [letter for letter in letters_remaining_count if letters_remaining_count[letter] == 0])

            first_letter = input("Enter first letter: ").lower()

            # as in the game only the first letter of the word has any significance,
            # we search for any word that starts with the given letter
            if len(first_letter) == 1 and first_letter.isalpha():
                requested_options = {index: word.replace("\n", "") for index, word in enumerate(options) if word.startswith(first_letter)}
                for index in requested_options:
                    print("{0}. {1}".format(index + 1, requested_options[index]))

                valid_remove_index = False
                remove_index = 0

                try:
                    # only accept an index of a word that starts with the requested letter
                    # or an ignore value (0/negative)
                    while not valid_remove_index:
                        remove_index = int(input("Enter used option (0 to ignore): "))
                        valid_remove_index = remove_index - 1 in requested_options or remove_index <= 0
                except ValueError:
                    print("Invalid input; treated as 0")
                    remove_index = 0

                # the program displays index+1, so we need to make sure to actually refer to the index
                remove_index -= 1

                # removing the option the user chose, if the user so desires
                if remove_index >= 0:
                    try:
                        removed_option = options.pop(remove_index)
                        letters_remaining_count[removed_option[0]] -= 1
                        removed_option = removed_option.replace("\n", "")
                    except IndexError:
                        print("Could not remove option {0}".format(remove_index + 1))
                    else:
                        print("Removed option {0} ({1})".format(remove_index + 1, removed_option))

            # waiting for input before clearing screen
            input("Press Enter to continue...")
            cls()

    except KeyboardInterrupt:
        print("\nBye Bye!")
    except Exception as e:
        print("Encountered unhandled exception: {0}".format(e))
    finally:

        # updating the user's file and closing any resources
        user_options_file.seek(0)
        for option in options:
            user_options_file.write(option)
            if not option.endswith("\n"):
                user_options_file.write("\n")

        user_options_file.truncate()

        user_options_file.close()


if __name__ == "__main__":
    main()
