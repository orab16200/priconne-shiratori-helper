# Priconne Shiritori Helper
A simple tool meant to help people complete the Dragon's Nest event's Shiritori minigame in the Princess Connect: Re Dive mobile game.

# How to Run
1. Make sure you have python 3.X on your device, with the following modules:
    -os
    -csv
2. Open cmd in the folder containing the main.py file
3. Run the following command: python main.py.

# Credits
The srt_panel.csv file was taken from https://github.com/FabulousCupcake/en_redive_master_db_diff.
